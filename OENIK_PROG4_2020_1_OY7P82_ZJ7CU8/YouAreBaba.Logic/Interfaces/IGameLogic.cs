﻿// <copyright file="IGameLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace YouAreBaba.Logic.Interfaces
{
    using System.Collections.Generic;
    using YouAreBaba.Model;
    using YouAreBaba.Model.Objects;

    /// <summary>
    /// Interface of GameLogic.
    /// </summary>
    public interface IGameLogic
    {
        /// <summary>
        /// Saves a map.
        /// </summary>
        /// <param name="currentMap">the id of the map to be saved.</param>
        /// <param name="currentlyPlayedMap">the map itself.</param>
        void AddSaveGame(int currentMap, List<MapObject> currentlyPlayedMap);

        /// <summary>
        /// This method is responsible for moving movable fields.
        /// </summary>
        /// <param name="currentlyPlayedMap">currently played map.</param>
        /// <param name="v1">the x value of the move.</param>
        /// <param name="v2">the y value of the move.</param>
        /// <returns>returns 1 if we die, returns 0 if we don't.</returns>
        int MoveEveryMovableObject(List<MapObject> currentlyPlayedMap, int v1, int v2);

        /// <summary>
        /// Refreshing mapmaking.
        /// </summary>
        /// <param name="currentlyPlayedMap">Currently played map.</param>
        /// <returns>Returns the map in a string matrix.</returns>
        string[,] NewTickSoFreshMapMaking(List<MapObject> currentlyPlayedMap);

        /// <summary>
        /// Collects the names of the pictures from the Pictures folder, where the appearance of each field is stored.
        /// </summary>
        /// <returns>All pictures names.</returns>
        List<string> ScanAllPictureNames();

        /// <summary>
        /// This method loads all the maps from the Maps folder.
        /// </summary>
        /// <returns>Returns all the maps.</returns>
        List<List<MapObject>> ScanAllMaps();

        /// <summary>
        /// Scans all frames of gif.
        /// </summary>
        /// <param name="temp">Current name of field.</param>
        /// <returns>Collects the names of the animation frames of the field passed in the string into a string array.</returns>
        List<string> ScanAllFramesOfGif(string temp);

        /// <summary>
        /// Victory on this map.
        /// </summary>
        /// <returns>2.</returns>
        int VictoryOnThisMap();

        /// <summary>
        /// Quicksave function.
        /// </summary>
        /// <returns>A list of MapObjects.</returns>
        List<MapObject> LoadGame();

        /// <summary>
        /// Smoothy move MapObjects on the map.
        /// </summary>
        /// <param name="cpm">The current map.</param>
        /// <param name="x">X coordinate.</param>
        /// <param name="y">Y coordinate.</param>
        /// <param name="thismuchmoved">The ID of the MapObject that sould be moved.</param>
        void MoveSmoothlyPixels(List<MapObject> cpm, int x, int y, int thismuchmoved);
    }
}
