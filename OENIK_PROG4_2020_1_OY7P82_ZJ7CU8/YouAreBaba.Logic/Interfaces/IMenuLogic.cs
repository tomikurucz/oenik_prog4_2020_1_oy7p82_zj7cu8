﻿// <copyright file="IMenuLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace YouAreBaba.Logic.Interfaces
{
    using System.Collections.Generic;
    using YouAreBaba.Model;

    /// <summary>
    /// Interface of MenuLogic.
    /// </summary>
    public interface IMenuLogic
    {
        /// <summary>
        /// Reads the dimensions of each maps from an external file.
        /// </summary>
        /// <returns>The size of all maps.</returns>
        List<int[]> ScanMapSizes();

        /// <summary>
        /// Get list of user data.
        /// </summary>
        /// <returns>The list of user data.</returns>
        List<Highscore> Get();

        /// <summary>
        /// Deletes all highscores for the user.
        /// </summary>
        /// <param name="name">Name of user.</param>
        void CleanLastPlayedMap(string name);

        /// <summary>
        /// Add a new user highscore.
        /// </summary>
        /// <param name="name">Name of the user.</param>
        /// <param name="mapid">ID of the map.</param>
        /// <param name="time">Played time.</param>
        /// <returns>Success or not.</returns>
        bool Add(string name, int mapid, int time);

        /// <summary>
        /// Deletes all highscores for the user.
        /// </summary>
        /// <param name="name">Name of user.</param>
        /// <returns>Success or not.</returns>
        bool DeleteAllRecord(string name);

        /// <summary>
        /// Collects the IDs of the user's winning maps.
        /// </summary>
        /// <param name="name">Name of user.</param>
        /// <returns>The IDs of the user's winning maps.</returns>
        List<int> GetAllVictoryMapNumberByUser(string name);

        /// <summary>
        /// Checking if user exists.
        /// </summary>
        /// <param name="name">Name of user.</param>
        /// <returns>If exited true, otherwise false.</returns>
        bool CheckUserExists(string name);

        /// <summary>
        /// Change last played map button to red.
        /// </summary>
        /// <param name="be">Selected button.</param>
        void ChangeLastPlayedMapButtonToRed(FluidButton be);

        /// <summary>
        /// Change last played map button to green.
        /// </summary>
        /// <param name="be">Selected button.</param>
        void ChangeLastPlayedMapButtonToGreen(FluidButton be);

        /// <summary>
        /// Change map button to green.
        /// </summary>
        /// <param name="be">Selected button.</param>
        void ChangeMapButtonToGreen(FluidButton be);

        /// <summary>
        /// Decides whether the user can play this map.
        /// </summary>
        /// <param name="username">Name of user.</param>
        /// <param name="mapid">ID of the map.</param>
        /// <returns>If the previous track is completed then true, otherwise false.</returns>
        bool CanThisMapBePlayedByUser(string username, int mapid);
    }
}
