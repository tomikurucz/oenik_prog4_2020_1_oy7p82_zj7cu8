﻿// <copyright file="IUserLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace YouAreBaba.Logic.Interfaces
{
    using System.Collections.Generic;

    /// <summary>
    /// Interface of UserLogic.
    /// </summary>
    public interface IUserLogic
    {
        /// <summary>
        /// Checking if user exists.
        /// </summary>
        /// <param name="name">Name of user.</param>
        /// <returns>If exited true, otherwise false.</returns>
        bool CheckUserExists(string name);

        /// <summary>
        /// Add a new user highscore.
        /// </summary>
        /// <param name="name">Name of the user.</param>
        /// <param name="mapid">ID of the map.</param>
        /// <param name="time">Played time.</param>
        /// <returns>Success or not.</returns>
        bool Add(string name, int mapid, int time);

        /// <summary>
        /// Collects all the names.
        /// </summary>
        /// <returns>A list if names.</returns>
        List<string> GetOnlyNmes();
    }
}
