﻿// <copyright file="MenuLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace YouAreBaba.Logic.Classes
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Media;
    using YouAreBaba.Logic.Interfaces;
    using YouAreBaba.Model;
    using YouAreBaba.Repositorys;

    /// <summary>
    /// class responsible for the logic of the menu.
    /// </summary>
    public class MenuLogic : IMenuLogic
    {
        private IRepository repository;
        private IDatabaseRepository repositoryDB;

        /// <summary>
        /// Initializes a new instance of the <see cref="MenuLogic"/> class.
        /// </summary>
        public MenuLogic()
        {
            this.repository = new Repository();
            this.repositoryDB = new DatabaseRepository();
        }

        /// <summary>
        /// Deletes all highscores for the user.
        /// </summary>
        /// <param name="name">Name of user.</param>
        public void CleanLastPlayedMap(string name)
        {
            this.repositoryDB.DeleteAllRecord(name);
        }

        /// <summary>
        /// Reads the dimensions of each maps from an external file.
        /// </summary>
        /// <returns>The size of all maps.</returns>
        public List<int[]> ScanMapSizes()
        {
            return this.repository.ScanMapSizes();
        }

        /// <summary>
        /// Get list of user data.
        /// </summary>
        /// <returns>The list of user data.</returns>
        public List<Highscore> Get()
        {
            return this.repositoryDB.Get();
        }

        /// <summary>
        /// Add a new user highscore.
        /// </summary>
        /// <param name="name">Name of the user.</param>
        /// <param name="mapid">ID of the map.</param>
        /// <param name="time">Played time.</param>
        /// <returns>Success or not.</returns>
        public bool Add(string name, int mapid, int time)
        {
            return this.repositoryDB.Add(name, mapid, time);
        }

        /// <summary>
        /// Deletes all highscores for the user.
        /// </summary>
        /// <param name="name">Name of user.</param>
        /// <returns>Success or not.</returns>
        public bool DeleteAllRecord(string name)
        {
            return this.repositoryDB.DeleteAllRecord(name);
        }

        /// <summary>
        /// Collects the IDs of the user's winning maps.
        /// </summary>
        /// <param name="name">Name of user.</param>
        /// <returns>The IDs of the user's winning maps.</returns>
        public List<int> GetAllVictoryMapNumberByUser(string name)
        {
            var outpu = new List<int>();
            var listofusers = this.Get();
            foreach (var x in listofusers)
            {
                if (x.Name.Equals(name))
                {
                    outpu.Add(x.Mapid);
                }
            }

            return outpu;
        }

        /// <summary>
        /// Checking if user exists.
        /// </summary>
        /// <param name="name">Name of user.</param>
        /// <returns>If exited true, otherwise false.</returns>
        public bool CheckUserExists(string name)
        {
            var listofusers = this.Get();
            foreach (var x in listofusers)
            {
                if (x.Name.Equals(name))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Change last played map button to red.
        /// </summary>
        /// <param name="be">Selected button.</param>
        public void ChangeLastPlayedMapButtonToRed(FluidButton be)
        {
            be.Background = new SolidColorBrush(Colors.Red);
        }

        /// <summary>
        /// Change last played map button to green.
        /// </summary>
        /// <param name="be">Selected button.</param>
        public void ChangeLastPlayedMapButtonToGreen(FluidButton be)
        {
            be.Background = new SolidColorBrush(Colors.Green);
        }

        /// <summary>
        /// Change map button to green.
        /// </summary>
        /// <param name="be">Selected button.</param>
        public void ChangeMapButtonToGreen(FluidButton be)
        {
            be.Background = new SolidColorBrush(Colors.Green);
        }

        /// <summary>
        /// Decides whether the user can play this map.
        /// </summary>
        /// <param name="username">Name of user.</param>
        /// <param name="mapid">ID of the map.</param>
        /// <returns>If the previous track is completed then true, otherwise false.</returns>
        public bool CanThisMapBePlayedByUser(string username, int mapid)
        {
            var temp = this.GetAllVictoryMapNumberByUser(username);
            if (temp.Count() == 0)
            {
                temp.Add(0);
            }
            else
            {
                temp.Add(temp.Max() + 1);
            }

            if (temp.Contains(mapid))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
