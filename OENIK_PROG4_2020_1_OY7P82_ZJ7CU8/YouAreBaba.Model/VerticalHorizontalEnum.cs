﻿// <copyright file="VerticalHorizontalEnum.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace YouAreBaba.Model
{
    /// <summary>
    /// Decides whether the rule is vertical or horizontal on the map.
    /// </summary>
    public enum VerticalHorizontalEnum
    {
        /// <summary>
        /// Y axis.
        /// </summary>
        VERTICAL,

        /// <summary>
        /// X axis.
        /// </summary>
        HORIZONTAL,
    }
}
