﻿// <copyright file="MapObject.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace YouAreBaba.Model
{
    using System.Collections.Generic;
    using System.Windows;

    /// <summary>
    /// Tha main object on the game map.
    /// </summary>
    public class MapObject
    {
        private Rect area;

        /// <summary>
        /// Initializes a new instance of the <see cref="MapObject"/> class.
        /// </summary>
        /// <param name="x">X axis pixel position.</param>
        /// <param name="y">Y axis pixel position.</param>
        /// <param name="width">Pixel width.</param>
        /// <param name="height">Pixel height.</param>
        /// <param name="thisenum">The property of the object.</param>
        public MapObject(int x, int y, double width, double height, MapObjectEnum thisenum)
        {
            this.Dx = x;
            this.Dy = y;
            this.area = new Rect(this.Dx, this.Dy, width, height);
            this.MyProperty = thisenum;
            this.RelevantBehaviors = new List<MapObjectEnum>();
            this.MovedInThisTick = false;
        }

        /// <summary>
        /// Gets or sets the degree of change on the x-axis.
        /// </summary>
        public int Dx { get; set; }

        /// <summary>
        /// Gets or sets the degree of change on the y-axis.
        /// </summary>
        public int Dy { get; set; }

        /// <summary>
        /// Gets or sets the rectangle that represents the shape.
        /// </summary>
        public Rect Area { get => this.area; set => this.area = value; }

        /// <summary>
        /// Gets or sets the main property.
        /// </summary>
        public MapObjectEnum MyProperty { get; set; }

        /// <summary>
        /// Gets or sets all the current behaviours ina  tick.
        /// </summary>
        public List<MapObjectEnum> RelevantBehaviors { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this object been moved in this tick.
        /// </summary>
        public bool MovedInThisTick { get; set; }

        /// <summary>
        /// Sets MovedInThisTick true.
        /// </summary>
        public void MoveInThisTick()
        {
            this.MovedInThisTick = true;
        }

        /// <summary>
        /// Sets MovedInThisTick false.
        /// </summary>
        public void NotMoveInThisTick()
        {
            this.MovedInThisTick = false;
        }

        /// <summary>
        /// Moves shape by dx on the x-axis and dy on the y-axis.
        /// </summary>
        /// <param name="dx">Degree of movement on the x-axis.</param>
        /// <param name="dy">Degree of mevement on the y-axi.</param>
        public void Move(double dx, double dy)
        {
            this.area.X += dx;
            this.area.Y += dy;
        }
    }
}
