﻿// <copyright file="SeriateableMapObject.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace YouAreBaba.Model
{
    using System.Collections.Generic;
    using System.Windows;

    /// <summary>
    /// MapObject but in a seriable format.
    /// </summary>
    public class SeriateableMapObject
    {
        /// <summary>
        /// Gets or sets x coordinate.
        /// </summary>
        public int Dx { get; set; }

        /// <summary>
        /// Gets or sets y coordinate.
        /// </summary>
        public int Dy { get; set; }

        /// <summary>
        /// Gets or sets the area of the object.
        /// </summary>
        public Rect Area { get; set; }

        /// <summary>
        /// Gets or sets the type of the object.
        /// </summary>
        public MapObjectEnum MyProperty { get; set; }

        /// <summary>
        /// Gets or sets list of current object behaviours.
        /// </summary>
        public List<MapObjectEnum> RelevantBehaviors { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether has been this object moved in this Move.
        /// </summary>
        public bool MovedInThisTick { get; set; }
    }
}
