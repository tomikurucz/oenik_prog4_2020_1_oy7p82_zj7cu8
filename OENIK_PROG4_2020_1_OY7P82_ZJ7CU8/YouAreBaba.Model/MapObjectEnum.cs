﻿// <copyright file="MapObjectEnum.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace YouAreBaba.Model
{
    /// <summary>
    /// All the possible MapObject enums.
    /// </summary>
    public enum MapObjectEnum
    {
        /// <summary>
        /// MapObject: BABA.
        /// </summary>
        BABA,

        /// <summary>
        /// MapObject: BABA_WORD.
        /// </summary>
        BABA_WORD,

        /// <summary>
        /// MapObject: EMPTY.
        /// </summary>
        EMPTY, // fekete háttér, amin mozoghatsz !!! vagyis ezt cska tároljuk a mátrixban de a helyén semmit nem rajzolunk ki! (alapból a háttér színe lesz helyén)

        /// <summary>
        /// MapObject: https://babaiswiki.fandom.com/wiki/EMPTY.
        /// </summary>
        EMPTY_WORD,

        /// <summary>
        /// MapObject: WALL.
        /// </summary>
        WALL,

        /// <summary>
        /// MapObject: KERET.
        /// </summary>
        KERET,

        /// <summary>
        /// MapObject: WALL_WORD.
        /// </summary>
        WALL_WORD,

        /// <summary>
        /// MapObject: https://babaiswiki.fandom.com/wiki/WATER.
        /// </summary>
        WATER,

        /// <summary>
        /// MapObject: WATER_WORD.
        /// </summary>
        WATER_WORD,

        /// <summary>
        /// MapObject: https://babaiswiki.fandom.com/wiki/SKULL.
        /// </summary>
        SKULL,

        /// <summary>
        /// MapObject: SKULL_WORD.
        /// </summary>
        SKULL_WORD,

        /// <summary>
        /// MapObject: https://babaiswiki.fandom.com/wiki/FENCE.
        /// </summary>
        FENCE,

        /// <summary>
        /// MapObject: FENCE_WORD.
        /// </summary>
        FENCE_WORD,

        /// <summary>
        /// MapObject: https://babaiswiki.fandom.com/wiki/HEDGE.
        /// </summary>
        HEDGE,

        /// <summary>
        /// MapObject: HEDGE_WORD.
        /// </summary>
        HEDGE_WORD,

        /// <summary>
        /// MapObject: https://babaiswiki.fandom.com/wiki/TREE.
        /// </summary>
        TREE,

        /// <summary>
        /// MapObject: TREE_WORD.
        /// </summary>
        TREE_WORD,

        /// <summary>
        /// MapObject: https://babaiswiki.fandom.com/wiki/FLAG.
        /// </summary>
        FLAG,

        /// <summary>
        /// MapObject: FLAG_WORD.
        /// </summary>
        FLAG_WORD,

        /// <summary>
        /// MapObject: https://babaiswiki.fandom.com/wiki/KEY.
        /// </summary>
        KEY,

        /// <summary>
        /// MapObject: KEY_WORD.
        /// </summary>
        KEY_WORD,

        /// <summary>
        /// MapObject: https://babaiswiki.fandom.com/wiki/Grass_Yard.
        /// </summary>
        GRASS,

        /// <summary>
        /// MapObject: GRASS_WORD.
        /// </summary>
        GRASS_WORD,

        /// <summary>
        /// MapObject: https://babaiswiki.fandom.com/wiki/KEKE.
        /// </summary>
        KEKE,

        /// <summary>
        /// MapObject: KEKE_WORD.
        /// </summary>
        KEKE_WORD,

        /// <summary>
        /// MapObject: https://babaiswiki.fandom.com/wiki/ICE.
        /// </summary>
        ICE,

        /// <summary>
        /// MapObject: ICE_WORD.
        /// </summary>
        ICE_WORD,

        /// <summary>
        /// MapObject: https://babaiswiki.fandom.com/wiki/BELT.
        /// </summary>
        BELT,

        /// <summary>
        /// MapObject: BELT_WORD.
        /// </summary>
        BELT_WORD,

        /// <summary>
        /// MapObject: https://babaiswiki.fandom.com/wiki/BOX.
        /// </summary>
        BOX,

        /// <summary>
        /// MapObject: BOX_WORD.
        /// </summary>
        BOX_WORD,

        /// <summary>
        /// MapObject: https://babaiswiki.fandom.com/wiki/LEVEL.
        /// </summary>
        LEVEL_WORD,

        /// <summary>
        /// MapObject: https://babaiswiki.fandom.com/wiki/LAVA.
        /// </summary>
        LAVA,

        /// <summary>
        /// MapObject: LAVA_WORD.
        /// </summary>
        LAVA_WORD,

        /// <summary>
        /// MapObject: https://babaiswiki.fandom.com/wiki/DOOR.
        /// </summary>
        DOOR,

        /// <summary>
        /// MapObject: DOOR_WORD.
        /// </summary>
        DOOR_WORD,

        /// <summary>
        /// MapObject: https://babaiswiki.fandom.com/wiki/ROCK.
        /// </summary>
        ROCK,

        /// <summary>
        /// MapObject: ROCK_WORD.
        /// </summary>
        ROCK_WORD,

        /// <summary>
        /// MapObject: NEEDTOMOVE
        /// Pixel moving helper flag.
        /// </summary>
        NEEDTOMOVE,

        /// <summary>
        /// MapObject: ONLYMOVEONCE.
        /// </summary>
        ONLYMOVEONCE,

        /// <summary>
        /// MapObject: IS.
        /// </summary>
        IS,

        /// <summary>
        /// MapObject: YOU.
        /// Something that exists on the board has to be YOU or you fail the level. Examples: BABA IS YOU, WALL IS YOU, LEAF IS YOU, etc.
        /// </summary>
        YOU,

        /// <summary>
        /// MapObject: WIN.
        /// Either YOU have to stand on whichever item is WIN or whichever item you are has to also be WIN. This is how you clear a level. Examples: BABA IS YOU and FLAG IS WIN — stand on the flag to win. Or BABA IS YOU AND BABA IS WIN.
        /// </summary>
        WIN,

        /// <summary>
        /// MapObject: BABA_WORD.
        /// This means the object can be pushed. Example: If ROCK IS PUSH, you can push rocks.
        /// </summary>
        PUSH,

        /// <summary>
        /// MapObject: BABA_WORD.
        /// </summary>
        STOP, // This item prevents you from moving yourself or anything else through it. Example: If WALL IS STOP, you can’t go through walls or push any words or other objects through it.

        /// <summary>
        /// MapObject: SINK.
        /// Touching an item that’s SINK will make both you and it disappear. Examples: WATER IS SINK — If you touch water, you will both disappear. Or if BABA IS SINK, anything you touch will cause both of you to disappear.
        /// </summary>
        SINK,

        /// <summary>
        /// MapObject: DEFEAT.
        /// If you touch this item, you disappear. If there was only one of you, you also fail the level. Example: If SKULL IS DEFEAT, then you’ll die when you touch a skull.
        /// </summary>
        DEFEAT,

        /// <summary>
        /// MapObject: MOVE.
        /// This makes an object move on its own in the direction it’s facing. If it hits an obstacle, it will turn around and walk the other way. You can make it move without you by pressing the spacebar (or A on Switch) to wait. Paired with YOU, it makes you do a double-move that allows you to walk over thinks that normally would kill you. Example: If KEKE IS MOVE, then Keke will walk on its own as you move or as you wait.
        /// </summary>
        MOVE,

        /// <summary>
        /// MapObject: SHUT.
        /// </summary>
        SHUT,

        /// <summary>
        /// MapObject: OPEN.
        /// If something is SHUT, it will disappear when something that is OPEN touches it, and so will the other object. Usually SHUT comes with STOP, but pay close attention, as SHUT on its own doesn’t stop you. Example: If DOOR IS SHUT and KEY IS OPEN, then pushing the key onto the door will make both the door and key disappear.
        /// </summary>
        OPEN,

        /// <summary>
        /// MapObject: WEAK.
        /// You can jump over
        /// </summary>
        WEAK,

        /// <summary>
        /// MapObject: SWAP.
        /// If something is SWAP, it will change places with anything it touches. Example: If BABA IS SWAP, then Baba can’t land on the flag, because he’ll swap with it. He’ll also swap with any text he runs into.
        /// </summary>
        SWAP,
    }
}
