var class_you_are_baba_1_1_logic_1_1_game_logic =
[
    [ "GameLogic", "class_you_are_baba_1_1_logic_1_1_game_logic.html#abb60bd21f16743a25a14c965d377de41", null ],
    [ "AddSaveGame", "class_you_are_baba_1_1_logic_1_1_game_logic.html#ada95d635f0504525a1e735b3db03f9e4", null ],
    [ "LoadGame", "class_you_are_baba_1_1_logic_1_1_game_logic.html#a321b647af6ab63c9f38603e4c4fb119f", null ],
    [ "MoveEveryMovableObject", "class_you_are_baba_1_1_logic_1_1_game_logic.html#a36df7683a98982fa7a8e4873bc1fc7f4", null ],
    [ "MoveSmoothlyPixels", "class_you_are_baba_1_1_logic_1_1_game_logic.html#a491425fd3cee378cb522e52448954281", null ],
    [ "NewTickSoFreshMapMaking", "class_you_are_baba_1_1_logic_1_1_game_logic.html#a777b885e12027d625412f2a0378ba587", null ],
    [ "ScanAllFramesOfGif", "class_you_are_baba_1_1_logic_1_1_game_logic.html#af88b590c0dbe12e9c0082cd86f9c36f5", null ],
    [ "ScanAllMaps", "class_you_are_baba_1_1_logic_1_1_game_logic.html#a9b8a375ff75581cef9cf9bb9f4d95739", null ],
    [ "ScanAllPictureNames", "class_you_are_baba_1_1_logic_1_1_game_logic.html#a61eecce57c218c77dd7b58cf3b4fd095", null ],
    [ "VictoryOnThisMap", "class_you_are_baba_1_1_logic_1_1_game_logic.html#a67ee257eae9cb253ad2e30a9457044ff", null ],
    [ "Currentrulesofthegame", "class_you_are_baba_1_1_logic_1_1_game_logic.html#a3561c34f7dc2d00fdbc351f3e7131149", null ],
    [ "PreviousStateOfcpm", "class_you_are_baba_1_1_logic_1_1_game_logic.html#abeadc207a80a9273b134bd36edf90035", null ]
];