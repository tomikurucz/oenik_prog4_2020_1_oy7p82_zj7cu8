var interface_you_are_baba_1_1_logic_1_1_interfaces_1_1_i_menu_logic =
[
    [ "Add", "interface_you_are_baba_1_1_logic_1_1_interfaces_1_1_i_menu_logic.html#a92afd2ad27aa559df18de63179839397", null ],
    [ "CanThisMapBePlayedByUser", "interface_you_are_baba_1_1_logic_1_1_interfaces_1_1_i_menu_logic.html#ad54bede30caec7e8122ce94e48db4e8a", null ],
    [ "ChangeLastPlayedMapButtonToGreen", "interface_you_are_baba_1_1_logic_1_1_interfaces_1_1_i_menu_logic.html#a5a985b85c776ecd3c1bb08cabaa674eb", null ],
    [ "ChangeLastPlayedMapButtonToRed", "interface_you_are_baba_1_1_logic_1_1_interfaces_1_1_i_menu_logic.html#a1cd9eb819deacf07f17459212e7a1fa7", null ],
    [ "ChangeMapButtonToGreen", "interface_you_are_baba_1_1_logic_1_1_interfaces_1_1_i_menu_logic.html#a55427bee0eefbd1d01d672310a49fcea", null ],
    [ "CheckUserExists", "interface_you_are_baba_1_1_logic_1_1_interfaces_1_1_i_menu_logic.html#af51aaba5a3ee1d4e50f0cb8fcb991017", null ],
    [ "CleanLastPlayedMap", "interface_you_are_baba_1_1_logic_1_1_interfaces_1_1_i_menu_logic.html#af2dc2d5d6dba5b133134afd263316c82", null ],
    [ "DeleteAllRecord", "interface_you_are_baba_1_1_logic_1_1_interfaces_1_1_i_menu_logic.html#a3b57506a498657d7a626c6620a815e5e", null ],
    [ "Get", "interface_you_are_baba_1_1_logic_1_1_interfaces_1_1_i_menu_logic.html#ab1076f31a1d0b2d7566857f154fa81bf", null ],
    [ "GetAllVictoryMapNumberByUser", "interface_you_are_baba_1_1_logic_1_1_interfaces_1_1_i_menu_logic.html#a3528e24f58491cd595cf5b8f78da76be", null ],
    [ "ScanMapSizes", "interface_you_are_baba_1_1_logic_1_1_interfaces_1_1_i_menu_logic.html#a12d0d2661d91d1b31d80447bed86dfca", null ]
];