var interface_you_are_baba_1_1_logic_1_1_interfaces_1_1_i_game_logic =
[
    [ "AddSaveGame", "interface_you_are_baba_1_1_logic_1_1_interfaces_1_1_i_game_logic.html#a84dbbd65d57fae7a1778e96e3e9baef3", null ],
    [ "LoadGame", "interface_you_are_baba_1_1_logic_1_1_interfaces_1_1_i_game_logic.html#a343b588789a4e267d0e6043137694a3e", null ],
    [ "MoveEveryMovableObject", "interface_you_are_baba_1_1_logic_1_1_interfaces_1_1_i_game_logic.html#a7983351b01ff15dc26552f8de06d49c4", null ],
    [ "MoveSmoothlyPixels", "interface_you_are_baba_1_1_logic_1_1_interfaces_1_1_i_game_logic.html#a5b070b9f427f2c5fb74c28adff0f5a58", null ],
    [ "NewTickSoFreshMapMaking", "interface_you_are_baba_1_1_logic_1_1_interfaces_1_1_i_game_logic.html#aff9d11c3c1746f52b8b4666b207a3246", null ],
    [ "ScanAllFramesOfGif", "interface_you_are_baba_1_1_logic_1_1_interfaces_1_1_i_game_logic.html#af019450be2485ce615c682d22dadf691", null ],
    [ "ScanAllMaps", "interface_you_are_baba_1_1_logic_1_1_interfaces_1_1_i_game_logic.html#a232d151627ab0198d44924fcd4d127d5", null ],
    [ "ScanAllPictureNames", "interface_you_are_baba_1_1_logic_1_1_interfaces_1_1_i_game_logic.html#a5374bdf70b47eb55f869d96f76702b04", null ],
    [ "VictoryOnThisMap", "interface_you_are_baba_1_1_logic_1_1_interfaces_1_1_i_game_logic.html#a4e54b5d435267f520f2ac4e122245b12", null ]
];