var hierarchy =
[
    [ "Application", null, [
      [ "YouAreBaba.View.App", "class_you_are_baba_1_1_view_1_1_app.html", null ]
    ] ],
    [ "YouAreBaba.Model.FluidButton", "class_you_are_baba_1_1_model_1_1_fluid_button.html", null ],
    [ "FrameworkElement", null, [
      [ "YouAreBaba.View.GameScreen", "class_you_are_baba_1_1_view_1_1_game_screen.html", null ]
    ] ],
    [ "YouAreBaba.Model.Highscore", "class_you_are_baba_1_1_model_1_1_highscore.html", null ],
    [ "IComponentConnector", null, [
      [ "YouAreBaba.View.UserControls.ChoosePlayer", "class_you_are_baba_1_1_view_1_1_user_controls_1_1_choose_player.html", null ],
      [ "YouAreBaba.View.UserControls.CurrentyPlayedGame", "class_you_are_baba_1_1_view_1_1_user_controls_1_1_currenty_played_game.html", null ],
      [ "YouAreBaba.View.UserControls.FrontMenu", "class_you_are_baba_1_1_view_1_1_user_controls_1_1_front_menu.html", null ],
      [ "YouAreBaba.View.UserControls.HighscoreMenu", "class_you_are_baba_1_1_view_1_1_user_controls_1_1_highscore_menu.html", null ],
      [ "YouAreBaba.View.UserControls.MapChooserMenu", "class_you_are_baba_1_1_view_1_1_user_controls_1_1_map_chooser_menu.html", null ],
      [ "YouAreBaba.View.UserControls.RulesAndControlsMenu", "class_you_are_baba_1_1_view_1_1_user_controls_1_1_rules_and_controls_menu.html", null ]
    ] ],
    [ "YouAreBaba.Repositorys.IDatabaseRepository", "interface_you_are_baba_1_1_repositorys_1_1_i_database_repository.html", [
      [ "YouAreBaba.Repositorys.DatabaseRepository", "class_you_are_baba_1_1_repositorys_1_1_database_repository.html", null ]
    ] ],
    [ "YouAreBaba.Logic.Interfaces.IGameLogic", "interface_you_are_baba_1_1_logic_1_1_interfaces_1_1_i_game_logic.html", [
      [ "YouAreBaba.Logic.GameLogic", "class_you_are_baba_1_1_logic_1_1_game_logic.html", null ]
    ] ],
    [ "YouAreBaba.ViewModel.IGameViewModel", "interface_you_are_baba_1_1_view_model_1_1_i_game_view_model.html", [
      [ "YouAreBaba.ViewModel.GameViewModel", "class_you_are_baba_1_1_view_model_1_1_game_view_model.html", null ]
    ] ],
    [ "YouAreBaba.Logic.Interfaces.IMenuLogic", "interface_you_are_baba_1_1_logic_1_1_interfaces_1_1_i_menu_logic.html", [
      [ "YouAreBaba.Logic.Classes.MenuLogic", "class_you_are_baba_1_1_logic_1_1_classes_1_1_menu_logic.html", null ]
    ] ],
    [ "YouAreBaba.ViewModel.IMenuViewModel", "interface_you_are_baba_1_1_view_model_1_1_i_menu_view_model.html", [
      [ "YouAreBaba.ViewModel.MenuViewModel", "class_you_are_baba_1_1_view_model_1_1_menu_view_model.html", null ]
    ] ],
    [ "InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "YouAreBaba.Repositorys.IRepository", "interface_you_are_baba_1_1_repositorys_1_1_i_repository.html", [
      [ "YouAreBaba.Repositorys.Repository", "class_you_are_baba_1_1_repositorys_1_1_repository.html", null ]
    ] ],
    [ "IStyleConnector", null, [
      [ "YouAreBaba.View.UserControls.MapChooserMenu", "class_you_are_baba_1_1_view_1_1_user_controls_1_1_map_chooser_menu.html", null ]
    ] ],
    [ "YouAreBaba.Logic.Interfaces.IUserLogic", "interface_you_are_baba_1_1_logic_1_1_interfaces_1_1_i_user_logic.html", [
      [ "YouAreBaba.Logic.Classes.UserLogic", "class_you_are_baba_1_1_logic_1_1_classes_1_1_user_logic.html", null ]
    ] ],
    [ "YouAreBaba.ViewModel.IUserViewModel", "interface_you_are_baba_1_1_view_model_1_1_i_user_view_model.html", [
      [ "YouAreBaba.ViewModel.UserViewModel", "class_you_are_baba_1_1_view_model_1_1_user_view_model.html", null ]
    ] ],
    [ "YouAreBaba.Model.MapObject", "class_you_are_baba_1_1_model_1_1_map_object.html", null ],
    [ "YouAreBaba.View.MusicFactory", "class_you_are_baba_1_1_view_1_1_music_factory.html", null ],
    [ "YouAreBaba.Model.Objects.RuleObject", "class_you_are_baba_1_1_model_1_1_objects_1_1_rule_object.html", null ],
    [ "YouAreBaba.Model.SeriateableMapObject", "class_you_are_baba_1_1_model_1_1_seriateable_map_object.html", null ],
    [ "UserControl", null, [
      [ "YouAreBaba.View.UserControls.ChoosePlayer", "class_you_are_baba_1_1_view_1_1_user_controls_1_1_choose_player.html", null ],
      [ "YouAreBaba.View.UserControls.CurrentyPlayedGame", "class_you_are_baba_1_1_view_1_1_user_controls_1_1_currenty_played_game.html", null ],
      [ "YouAreBaba.View.UserControls.FrontMenu", "class_you_are_baba_1_1_view_1_1_user_controls_1_1_front_menu.html", null ],
      [ "YouAreBaba.View.UserControls.HighscoreMenu", "class_you_are_baba_1_1_view_1_1_user_controls_1_1_highscore_menu.html", null ],
      [ "YouAreBaba.View.UserControls.MapChooserMenu", "class_you_are_baba_1_1_view_1_1_user_controls_1_1_map_chooser_menu.html", null ],
      [ "YouAreBaba.View.UserControls.RulesAndControlsMenu", "class_you_are_baba_1_1_view_1_1_user_controls_1_1_rules_and_controls_menu.html", null ]
    ] ],
    [ "ViewModelBase", null, [
      [ "YouAreBaba.ViewModel.MenuViewModel", "class_you_are_baba_1_1_view_model_1_1_menu_view_model.html", null ],
      [ "YouAreBaba.ViewModel.UserViewModel", "class_you_are_baba_1_1_view_model_1_1_user_view_model.html", null ]
    ] ],
    [ "Window", null, [
      [ "YouAreBaba.View.MainWindow", "class_you_are_baba_1_1_view_1_1_main_window.html", null ]
    ] ]
];