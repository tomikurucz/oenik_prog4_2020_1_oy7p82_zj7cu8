﻿// <copyright file="CurrentyPlayedGame.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace YouAreBaba.View.UserControls
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using YouAreBaba.ViewModel;

    /// <summary>
    /// Interaction logic for CurrentyPlayedGame.xaml.
    /// </summary>
    public partial class CurrentyPlayedGame : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CurrentyPlayedGame"/> class.
        /// </summary>
        public CurrentyPlayedGame()
        {
            if (!(Application.Current.MainWindow.DataContext as MenuViewModel).GameViewhasbeenloaded)
            {
                (Application.Current.MainWindow.DataContext as MenuViewModel).GameViewhasbeenloaded = true;
                this.InitializeComponent();
                this.MainGrid.Content = new GameScreen();
                (Application.Current.MainWindow.DataContext as MenuViewModel).SetMapForGameplay(
                    (this.MainGrid.Content as GameScreen).ActiveMapID,
                    (this.MainGrid.Content as GameScreen).ObjectPixel);
                this.InvalidateVisual();
                Console.WriteLine("CurrentyPlayedGame");
            }
        }
    }
}
