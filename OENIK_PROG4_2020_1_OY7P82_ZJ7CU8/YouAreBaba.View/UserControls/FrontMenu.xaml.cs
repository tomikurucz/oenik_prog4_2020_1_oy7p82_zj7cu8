﻿// <copyright file="FrontMenu.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace YouAreBaba.View.UserControls
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using YouAreBaba.ViewModel;

    /// <summary>
    /// Interaction logic for FrontMenu.xaml.
    /// </summary>
    public partial class FrontMenu : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FrontMenu"/> class.
        /// </summary>
        public FrontMenu()
        {
            this.InitializeComponent();
            (Application.Current.MainWindow.DataContext as MenuViewModel).PositionHeight = (1080 / 2) - 300;
            (Application.Current.MainWindow.DataContext as MenuViewModel).PositionWidth = (1920 / 2) - 400;
            (Application.Current.MainWindow.DataContext as MenuViewModel).ChangeVictoriousPlayedMapButtonToGreen();
        }

        private void Choose_Map_Button_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("FrontMenu Choose_Map_Button_Click SwitchView");
            (Window.GetWindow(this).DataContext as MenuViewModel).SwitchView = 2;
        }

        private void Exit_Button_Click(object sender, RoutedEventArgs e)
        {
            Window.GetWindow(this).Close();
        }

        private void Rules_Button_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("FrontMenu Rules_Button_Click SwitchView");
            (Window.GetWindow(this).DataContext as MenuViewModel).SwitchView = 3;
        }

        private void Load_Button_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("FrontMenu Load_Button_Click SwitchView");
            (Window.GetWindow(this).DataContext as MenuViewModel).SwitchView = 1;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            (Window.GetWindow(this).DataContext as MenuViewModel).CleanLastPlayedMap((Window.GetWindow(this).DataContext as MenuViewModel).GamerName);
            (Window.GetWindow(this).DataContext as MenuViewModel).ChangeAllPlayedMapButtonToNothing();
            MessageBox.Show("Highscore deleted");
        }

        private void Highscore_Click(object sender, RoutedEventArgs e)
        {
            (Window.GetWindow(this).DataContext as MenuViewModel).SwitchView = 4;
        }

        private void Change_Click(object sender, RoutedEventArgs e)
        {
            (Window.GetWindow(this).DataContext as MenuViewModel).SwitchView = 5;
        }
    }
}
