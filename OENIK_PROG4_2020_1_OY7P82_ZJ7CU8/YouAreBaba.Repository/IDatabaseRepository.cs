﻿// <copyright file="IDatabaseRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace YouAreBaba.Repositorys
{
    using System.Collections.Generic;
    using YouAreBaba.Model;

    /// <summary>
    /// Interface of user data repository.
    /// </summary>
    public interface IDatabaseRepository
    {
        /// <summary>
        /// Get list of user data.
        /// </summary>
        /// <returns>The list of user data.</returns>
        List<Highscore> Get();

        /// <summary>
        /// Add a new user highscore.
        /// </summary>
        /// <param name="name">Name of the user.</param>
        /// <param name="mapid">ID of the map.</param>
        /// <param name="time">Played time.</param>
        /// <returns>Success or not.</returns>
        bool Add(string name, int mapid, int time);

        /// <summary>
        /// Deletes all highscores for the user.
        /// </summary>
        /// <param name="name">Name of user.</param>
        /// <returns>Success or not.</returns>
        bool DeleteAllRecord(string name);
    }
}
