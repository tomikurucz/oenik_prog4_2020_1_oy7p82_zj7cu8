﻿// <copyright file="Repository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace YouAreBaba.Repositorys
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml.Serialization;
    using YouAreBaba.Model;

    /// <summary>
    /// The main Repository class.
    /// </summary>
    public class Repository : IRepository
    {
        private int width;
        private int height;
        private int objectPixel;

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository"/> class.
        /// </summary>
        /// <param name="objectPixel">The size of one MapObject.</param>
        /// <param name="width">Width of map.</param>
        /// <param name="height">Height of map.</param>
        public Repository(int objectPixel, int width, int height)
        {
            this.objectPixel = objectPixel;
            this.width = width;
            this.height = height;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository"/> class.
        /// </summary>
        public Repository()
        {
            this.objectPixel = 0;
            this.width = 0;
            this.height = 0;
        }

        /// <inheritdoc/>
        public List<string> ScanAllFramesOfGif(string temp)
        {
            List<string> kimenet = new List<string>();
            DirectoryInfo d = new DirectoryInfo(@"../../Pictures/" + temp);
            FileInfo[] files = d.GetFiles("*.png");
            foreach (FileInfo file in files)
            {
                kimenet.Add(file.Name.Substring(0, file.Name.IndexOf('.')));
            }

            return kimenet;
        }

        /// <inheritdoc/>
        public List<List<MapObject>> ScanAllMaps()
        {
            List<List<MapObject>> mapObjects = new List<List<MapObject>>();
            var files = Directory.EnumerateFiles(@"../../../YouAreBaba.Repository/Maps", "*.txt");
            int fileindex = 0;
            List<string[]> currentlyscannedmap;
            StreamReader sr;
            List<MapObject> tempcurrentlybuiltmap;

            foreach (string file in files)
            {
                using (FileStream fs = new FileStream(file, FileMode.Open, FileAccess.Read))
                {
                    currentlyscannedmap = new List<string[]>();
                    sr = new StreamReader(fs);
                    while (!sr.EndOfStream)
                    {
                        currentlyscannedmap.Add(sr.ReadLine().Split(','));
                    }

                    sr.Close();
                }

                tempcurrentlybuiltmap = new List<MapObject>();
                for (int j = 0; j < currentlyscannedmap.Count(); j++)
                {
                    for (int i = 0; i < currentlyscannedmap[j].Count(); i++)
                    {
                        var currentenum = Enum.Parse(typeof(MapObjectEnum), currentlyscannedmap[j][i]); // Amikor a MapObjecteket létrehozod a már megfelelő Dx és Dx pixeles koordinátákkal kell! Magasság és Szélesség fixen 24!
                        tempcurrentlybuiltmap.Add(new MapObject(this.objectPixel * i, this.objectPixel * j, this.objectPixel, this.objectPixel, (MapObjectEnum)currentenum));
                    }
                }

                mapObjects.Add(tempcurrentlybuiltmap);
                fileindex++;
            }

            return mapObjects;
        }

        /// <inheritdoc/>
        public List<string> ScanAllPictureNames()
        {
            List<string> kimenet = new List<string>();
            DirectoryInfo d = new DirectoryInfo(@"../../Pictures");
            FileInfo[] files = d.GetFiles("*.gif");
            foreach (FileInfo file in files)
            {
                kimenet.Add(file.Name.Substring(0, file.Name.IndexOf('.')));
            }

            return kimenet;
        }

        /// <inheritdoc/>
        public List<int[]> ScanMapSizes()
        {
            using (FileStream fs = new FileStream(@"../../../YouAreBaba.Repository/Maps/mapnumbers.csv", FileMode.Open, FileAccess.Read))
            {
                List<string[]> currentlyscannedmap = new List<string[]>();
                List<int[]> outbound = new List<int[]>();
                StreamReader sr = new StreamReader(fs);
                while (!sr.EndOfStream)
                {
                    currentlyscannedmap.Add(sr.ReadLine().Split(','));
                }

                sr.Close();
                foreach (var tex in currentlyscannedmap)
                {
                    outbound.Add(new int[] { Convert.ToInt32(tex[0]), Convert.ToInt32(tex[1]), Convert.ToInt32(tex[2]), Convert.ToInt32(tex[3]) });
                }

                return outbound;
            }
        }

        /// <inheritdoc/>
        public void AddSaveGame(int currentMap, List<MapObject> currentlyPlayedMap)
        {
            List<SeriateableMapObject> temp = new List<SeriateableMapObject>();
            foreach (var x in currentlyPlayedMap)
            {
                temp.Add(new SeriateableMapObject()
                {
                    Area = x.Area,
                    Dx = (int)x.Area.X,
                    Dy = (int)x.Area.Y,
                    MovedInThisTick = x.MovedInThisTick,
                    MyProperty = x.MyProperty,
                    RelevantBehaviors = x.RelevantBehaviors,
                });
            }

            string path = @"../../../YouAreBaba.Repository/SaveGame/quicksave";
            FileStream outFile = File.Create(path);
            XmlSerializer formatter = new XmlSerializer(typeof(List<SeriateableMapObject>));
            formatter.Serialize(outFile, temp);
        }

        /// <inheritdoc/>
        public List<MapObject> LoadGame()
        {
            string file = @"../../../YouAreBaba.Repository/SaveGame/quicksave";
            List<MapObject> listofa = new List<MapObject>();
            XmlSerializer formatter = new XmlSerializer(typeof(List<SeriateableMapObject>));
            FileStream aFile = new FileStream(file, FileMode.Open);
            byte[] buffer = new byte[aFile.Length];
            aFile.Read(buffer, 0, (int)aFile.Length);
            MemoryStream stream = new MemoryStream(buffer);
            var temp = (List<SeriateableMapObject>)formatter.Deserialize(stream);

            List<MapObject> currentlyPlayedMap = new List<MapObject>();
            foreach (var x in temp)
            {
                currentlyPlayedMap.Add(new MapObject(x.Dx, x.Dy, 36, 36, x.MyProperty)
                {
                    RelevantBehaviors = x.RelevantBehaviors,
                    MovedInThisTick = x.MovedInThisTick,
                });
            }

            return currentlyPlayedMap;
        }
    }
}
