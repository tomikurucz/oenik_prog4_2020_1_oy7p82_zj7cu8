﻿// <copyright file="UserViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace YouAreBaba.ViewModel
{
    using System.Collections.Generic;
    using GalaSoft.MvvmLight;
    using YouAreBaba.Logic.Classes;
    using YouAreBaba.Logic.Interfaces;

    /// <summary>
    /// The view model class for the user.
    /// </summary>
    public class UserViewModel : ViewModelBase, IUserViewModel
    {
        private string chosenUser;
        private List<string> usernames;
        private string fromListChosenUser;
        private bool boolfromListChosenUser;
        private bool boolchosenUser;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserViewModel"/> class.
        /// </summary>
        public UserViewModel()
        {
            this.ChosenUser = string.Empty;
            this.FromListChosenUser = string.Empty;
            this.FromListChosenUserBool = false;
            this.BoolchosenUser = false;
            this.Logic = new UserLogic();
            this.Usernames = this.Logic.GetOnlyNmes();
            this.FromListChosenUser = this.Usernames[0];
        }

        /// <inheritdoc/>
        public string ChosenUser
        {
            get { return this.chosenUser; }
            set { this.Set(ref this.chosenUser, value); }
        }

        /// <inheritdoc/>
        public List<string> Usernames
        {
            get { return this.usernames; }
            set { this.Set(ref this.usernames, value); }
        }

        /// <inheritdoc/>
        public string FromListChosenUser
        {
            get { return this.fromListChosenUser; }
            set { this.Set(ref this.fromListChosenUser, value); }
        }

        /// <inheritdoc/>
        public bool FromListChosenUserBool
        {
            get { return this.boolfromListChosenUser; }
            set { this.Set(ref this.boolfromListChosenUser, value); }
        }

        /// <inheritdoc/>
        public bool BoolchosenUser
        {
            get { return this.boolchosenUser; }
            set { this.Set(ref this.boolchosenUser, value); }
        }

        /// <summary>
        /// Gets get main logic for user interactions.
        /// </summary>
        public IUserLogic Logic
        {
            get;
        }

        /// <inheritdoc/>
        public bool CheckUserExists()
        {
            return this.Logic.CheckUserExists(this.ChosenUser);
        }

        /// <inheritdoc/>
        public bool Add(string name, int mapid, int time)
        {
            return this.Logic.Add(name, mapid, time);
        }
    }
}
