﻿// <copyright file="MenuViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace YouAreBaba.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Threading;
    using GalaSoft.MvvmLight;
    using YouAreBaba.Logic.Classes;
    using YouAreBaba.Logic.Interfaces;
    using YouAreBaba.Model;

    /// <summary>
    /// Class of the view model for the menu.
    /// </summary>
    public class MenuViewModel : ViewModelBase, IMenuViewModel
    {
        private int switchView;
        private bool gameviewhasbeenloaded;
        private int whichback;
        private List<BitmapImage> backMenu;
        private List<BitmapImage> backGame;
        private BitmapImage back;
        private DispatcherTimer timer;
        private int lastPlayedMap;
        private ObservableCollection<FluidButton> myButtons;
        private bool canLoad;
        private int positionHeight;
        private int positionWidth;
        private string gamerName;
        private List<string> gamerList;
        private bool menuOrGame;

        /// <summary>
        /// Initializes a new instance of the <see cref="MenuViewModel"/> class.
        /// </summary>
        public MenuViewModel()
        {
            this.positionHeight = (1080 / 2) - 300;
            this.positionWidth = (1920 / 2) - 400;

            this.Logic = new MenuLogic();
            this.GameViewhasbeenloaded = false;

            this.SwitchView = 5;
            this.AllMapsIDAndSizes = this.ScanMapSizes();

            this.FromSavedGameFinishedMap = this.Logic.GetAllVictoryMapNumberByUser(this.GamerName);

            this.backGame = new List<BitmapImage>();
            this.backMenu = new List<BitmapImage>();
            this.backMenu.Add(new BitmapImage(new Uri(@"../../Menu/menu/0.png", UriKind.Relative)));
            this.backMenu.Add(new BitmapImage(new Uri(@"../../Menu/menu/1.png", UriKind.Relative)));
            this.backMenu.Add(new BitmapImage(new Uri(@"../../Menu/menu/2.png", UriKind.Relative)));
            this.backGame.Add(new BitmapImage(new Uri(@"../../Menu/game/0.png", UriKind.Relative)));
            this.backGame.Add(new BitmapImage(new Uri(@"../../Menu/game/1.png", UriKind.Relative)));
            this.backGame.Add(new BitmapImage(new Uri(@"../../Menu/game/2.png", UriKind.Relative)));
            this.whichback = 0;
            this.MenuOrGame = true;
            this.timer = new DispatcherTimer();
            this.timer.Interval = TimeSpan.FromMilliseconds(200);
            this.timer.Tick += this.Timer_Back;
            this.timer.Start();

            this.Logo = new BitmapImage(new Uri(@"../../Menu/logo.gif", UriKind.Relative));
            this.Help = new BitmapImage(new Uri(@"../../Menu/Help.png", UriKind.Relative));
            this.MyButtons = new ObservableCollection<FluidButton>();
            this.AllPossibleMapCo = this.AllPossibleMapCoordinates();

            for (int i = 0; i < this.AllMapsIDAndSizes.Count; i++)
            {
                this.MyButtons.Add(new FluidButton(
                    this.AllMapsIDAndSizes[i][0] < 10 ? "0" + this.AllMapsIDAndSizes[i][0].ToString() : this.AllMapsIDAndSizes[i][0].ToString(),
                    this.AllPossibleMapCo[i][0] - 15,
                    this.AllPossibleMapCo[i][1] - 15,
                    this.AllMapsIDAndSizes[i][0]));
            }

            for (int i = 0; i < this.MyButtons.Count; i++)
            {
                if (this.FromSavedGameFinishedMap.Contains(i))
                {
                    this.MyButtons[i].Background = new SolidColorBrush(Colors.Green);
                }
            }
        }

        /// <inheritdoc/>
        public BitmapImage Logo { get; }

        /// <inheritdoc/>
        public BitmapImage Help { get; }

        /// <inheritdoc/>
        public List<int> FromSavedGameFinishedMap { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is the BAckground for menu or for game.
        /// </summary>
        public bool MenuOrGame
        {
            get { return this.menuOrGame; }
            set { this.Set(ref this.menuOrGame, value); }
        }

        /// <inheritdoc/>
        public bool CanLoad
        {
            get { return this.canLoad; }
            set { this.Set(ref this.canLoad, value); }
        }

        /// <inheritdoc/>
        public int PositionHeight
        {
            get { return this.positionHeight; }
            set { this.Set(ref this.positionHeight, value); }
        }

        /// <inheritdoc/>
        public int PositionWidth
        {
            get { return this.positionWidth; }
            set { this.Set(ref this.positionWidth, value); }
        }

        /// <inheritdoc/>
        public string GamerName
        {
            get { return this.gamerName; }
            set { this.Set(ref this.gamerName, value); }
        }

        /// <inheritdoc/>
        public int SwitchView
        {
            get { return this.switchView; }
            set { this.Set(ref this.switchView, value); }
        }

        /// <inheritdoc/>
        public BitmapImage Back
        {
            get { return this.back; }
            set { this.Set(ref this.back, value); }
        }

        /// <inheritdoc/>
        public int LastPlayedMap
        {
            get { return this.lastPlayedMap; }
            set { this.Set(ref this.lastPlayedMap, value); }
        }

        /// <summary>
        /// Gets or sets lIst of possible Gamers.
        /// </summary>
        public List<string> GamerList
        {
            get { return this.gamerList; }
            set { this.Set(ref this.gamerList, value); }
        }

        /// <inheritdoc/>
        public IMenuLogic Logic
        {
            get;
        }

        /// <inheritdoc/>
        public ObservableCollection<FluidButton> MyButtons
        {
            get { return this.myButtons; }
            set { this.Set(ref this.myButtons, value); }
        }

        /// <inheritdoc/>
        public bool GameViewhasbeenloaded
        {
            get { return this.gameviewhasbeenloaded; }
            set { this.Set(ref this.gameviewhasbeenloaded, value); }
        }

        private List<int[]> AllMapsIDAndSizes { get; set; }

        private List<int[]> AllPossibleMapCo { get; set; }

        /// <inheritdoc/>
        public void ChangeLastPlayedMapButtonToRed()
        {
            this.Logic.ChangeLastPlayedMapButtonToRed(this.MyButtons[this.LastPlayedMap]);
        }

        /// <inheritdoc/>
        public void ChangeLastPlayedMapButtonToGreen()
        {
            this.Logic.ChangeLastPlayedMapButtonToGreen(this.MyButtons[this.LastPlayedMap]);
        }

        /// <summary>
        /// Change the button of a map to green.
        /// </summary>
        /// <param name="d">ID of the map.</param>
        public void ChangeMapButtonToGreen(int d)
        {
            if (d <= this.AllPossibleMapCo.Count())
            {
                if (d < 14)
                {
                    this.Logic.ChangeMapButtonToGreen(this.MyButtons[d]);
                }
            }
        }

        /// <inheritdoc/>
        public void ChangeVictoriousPlayedMapButtonToGreen()
        {
            var listofalreadyfinishedmaps = this.Logic.GetAllVictoryMapNumberByUser(this.GamerName);
            foreach (var x in listofalreadyfinishedmaps)
            {
                this.ChangeMapButtonToGreen(x);
            }
        }

        /// <inheritdoc/>
        public void ChangeAllPlayedMapButtonToNothing()
        {
            foreach (var x in this.MyButtons)
            {
                x.Background = null;
            }
        }

        /// <summary>
        /// Delete all maps played earlier.
        /// </summary>
        /// <param name="name">Name of the map.</param>
        public void CleanLastPlayedMap(string name)
        {
            this.Logic.CleanLastPlayedMap(name);
        }

        /// <inheritdoc/>
        public List<int[]> ScanMapSizes()
        {
            return this.Logic.ScanMapSizes();
        }

        /// <inheritdoc/>
        public List<Highscore> Get()
        {
            return this.Logic.Get();
        }

        /// <summary>
        /// Add a new Highscore.
        /// </summary>
        /// <param name="name">Name of the player.</param>
        /// <param name="mapid">Map played ID.</param>
        /// <param name="time">Time spent on map.</param>
        /// <returns>Successfull or not.</returns>
        public bool Add(string name, int mapid, int time)
        {
            if (mapid <= this.AllMapsIDAndSizes.Count)
            {
                return this.Logic.Add(name, mapid, time);
            }
            else
            {
                return false;
            }
        }

        /// <inheritdoc/>
        public bool DeleteAllRecord(string name)
        {
            return this.Logic.DeleteAllRecord(name);
        }

        /// <inheritdoc/>
        public List<int> GetAllVictoryMapNumberByUser(string name)
        {
            return this.Logic.GetAllVictoryMapNumberByUser(name);
        }

        /// <inheritdoc/>
        public bool CheckUserExists(string name)
        {
            return this.Logic.CheckUserExists(name);
        }

        /// <summary>
        /// Check whether this map can be played by the user.
        /// </summary>
        /// <param name="mapid">ID of the map.</param>
        /// <returns>Can the user play it or not.</returns>
        public bool CanThisMapBePlayedByUser(int mapid)
        {
            return this.Logic.CanThisMapBePlayedByUser(this.GamerName, mapid);
        }

        /// <inheritdoc/>
        public void SetMapForGameplay(int mapid, int objectPixel)
        {
            this.PositionWidth = (21 * 24) + 8;
            this.PositionHeight = (8 * 24) + 5;
        }

        private void Timer_Back(object sender, EventArgs e)
        {
            if (this.whichback < 2)
            {
                this.whichback++;
            }
            else
            {
                this.whichback = 0;
            }

            if (this.MenuOrGame)
            {
                this.Back = this.backMenu[this.whichback];
            }
            else
            {
                this.Back = this.backGame[this.whichback];
            }
        }

        private List<int[]> AllPossibleMapCoordinates()
        {
            var temp = new List<int[]>
            {
                new int[] { 245, 241 },
                new int[] { 422, 284 },
                new int[] { 411, 132 },
                new int[] { 462, 453 },
                new int[] { 646, 493 },
                new int[] { 572, 310 },
                new int[] { 614, 156 },
                new int[] { 778, 253 },
                new int[] { 765, 424 },
                new int[] { 377, 381 },
                new int[] { 337, 482 },
                new int[] { 203, 475 },
                new int[] { 157, 370 },
                new int[] { 277, 144 },
                new int[] { 152, 190 },
                new int[] { 348, 256 },
                new int[] { 279, 518 },
                new int[] { 452, 370 },
                new int[] { 575, 473 },
                new int[] { 649, 269 },
                new int[] { 554, 219 },
            };
            return temp;
        }
    }
}
